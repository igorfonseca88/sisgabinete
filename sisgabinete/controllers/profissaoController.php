<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?php

class ProfissaoController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }
	
	function index()
	{
		$this->load->model('Profissao_model', 'profissao');
		$data["profissao"] = $this->profissao->getAll();
		
		$this->load->vars($data);
		$this->load->view("priv/profissao/listProfissao");
	}
	
	function novaProfissaoAction()
	{
		$this->load->view("priv/profissao/addProfissao");
	}
	
	function addProfissao()
	{
		$this->load->model("Profissao_model", "profissao");		
		$insert = array(
			"titulo" => $this->input->post("titulo")
		);
		
		if ($this->profissao->add_record($insert) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["error"] = "Erro ao salvar.";
		}
		$data["profissao"] = $this->profissao->getAll();
		$this->load->vars($data);
		$this->load->view("priv/profissao/listProfissao");
	}
	
	function deleteProfissao($id)
	{
		$this->load->model("Profissao_model", "profissao");		

		if ($this->profissao->delete($id) > 0) {
			$data["sucesso"] = "Excluído com sucesso.";
		} else {
			$data["error"] = "Erro ao excluir.";
		}
		
		$data["profissao"] = $this->profissao->getAll();
		$this->load->vars($data);
		$this->load->view("priv/profissao/listProfissao");
	}

	function editProfissao() {
		$this->load->model("Profissao_model", "profissao");
		$id = $this->input->post("id");
		
		$update = array(
			"titulo" => $this->input->post("titulo")
		);
		
		if ($this->profissao->update($id,$update) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["erro"] = "Erro ao salvar.";
		}
		
		$data["profissao"] = $this->profissao->getAll();
		$this->load->vars($data);
		$this->load->view("priv/profissao/listProfissao");
    }
    
    function editarProfissaoAction($id, $mensagem = array()) {   
        $this->load->model('Profissao_model', 'profissao');
        $data["profissao"] = $this->profissao->buscarPorId($id);
		
        $data["sucesso"] = $mensagem["sucesso"];
        $data["error"] = $mensagem["error"];
			   
        $this->load->vars($data);
        $this->load->view("priv/profissao/editProfissao");
    } 
}

?>