<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?php
class Agenda_model extends CI_Model {
    
    function buscarAgendaPorEmpresa($idEmpresa) {
        $sql = "select * from tb_agenda where idEmpresa = ? order by titulo asc";
		$query = $this->db->query($sql, array($idEmpresa));
        return $query->result();
    }
	
    function add_record($options = array()) {
        $this->db->insert('tb_agenda', $options);
        return $this->db->insert_id();
    }
    
    function update($id, $options = array()) {
        $this->db->where('id', $id);
        $this->db->update('tb_agenda',$options);
        return $this->db->affected_rows();
    }
    
    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('tb_agenda',$options);
        return $this->db->affected_rows();
    }
    
    function buscarPorId($id) {
        $this->db->where('id', $id);
        $query = $this->db->get("tb_agenda");
        return $query->result();
    }
}
?>