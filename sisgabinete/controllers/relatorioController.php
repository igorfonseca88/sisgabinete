<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?php

class RelatorioController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }
	
	function index()
	{
		
		$this->load->model('Assunto_model', 'assunto');
        $data["assunto"] = $this->assunto->getAll();
		
		$this->load->vars($data);
		$this->load->view("priv/relatorio/listRelatorio");
	}
	
	function gerarRelatorioAction()
	{
		$this->load->model('Pessoa_model', 'pessoa');
		$arrayPessoas = $this->pessoa->buscarPessoas();			
		
		$html = "<table width='80%'><tr><th>Nome</th><th>E-mail </th><th>Telefone</th></tr>";
		foreach ($arrayPessoas as $row) {
			$html = $html . "<tr><td>" . mb_convert_encoding($row->nome,'utf-16','utf-8') . "</td><td>" . $row->email . "</td><td>" . $row->telefone . "</td></tr>";		
		}
		$html = $html . "</table>";		
		
		header("Content-type: application/vnd.ms-excel");  // Determina que o arquivo é uma planilha do Excel		
		header("Content-type: application/force-download");  // Força o download do arquivo 
		header("Content-Disposition: attachment; filename=pessoas.xls"); // Seta o nome do arquivo		
		header("Pragma: no-cache");		
		echo $html;

	}
	
	function gerarRelatorioCompleto()
	{
		$this->load->model('Pessoa_model', 'pessoa');
		$arrayPessoas = $this->pessoa->buscarPessoaCompleto();			
		
		$html = "<table width='80%'><tr><th>Nome</th><th>E-mail </th><th>Data Nascimento</th><th>Telefone Fixo</th><th>Telefone Celular</th><th>Telefone Outro</th><th>CEP</th><th>Rua</th><th>Número</th><th>Bairro</th><th>Complemento</th><th>Cidade</th><th>Estado</th><th>Profissao</th><th>Referencia</th><th>Trabalho</th><th>Região</th></tr>";
		foreach ($arrayPessoas as $row) {
			if ($row->idregiao == 1) { $regiao = "Anhanduizinho"; }
			if ($row->idregiao == 4) { $regiao = "Bandeira"; }
			if ($row->idregiao == 6) { $regiao = "Imbirussu"; }
			if ($row->idregiao == 3) { $regiao = "Lagoa"; }
			if ($row->idregiao == 5) { $regiao = "Proza"; }
			if ($row->idregiao == 8) { $regiao = "Segredo"; }
			$html = $html . "<tr><td>" . mb_convert_encoding($row->nome,'utf-16','utf-8') . "</td><td>" . $row->email . "</td><td>" . $row->n . "</td><td>" . $row->nascimento . "</td><td>" . $row->n . "</td><td>" . $row->fixo . "</td><td>" . $row->n . "</td><td>" . $row->celular . "</td><td>" . $row->n . "</td><td>" . $row->outro . "</td><td>" . $row->n . "</td><td>" . $row->cep . "</td><td>" . $row->n . "</td><td>" . $row->rua . "</td><td>" . $row->n . "</td><td>" . $row->numero . "</td><td>" . $row->n . "</td><td>" . $row->bairro . "</td><td>" . $row->n . "</td><td>" . $row->complemento . "</td><td>" . $row->n . "</td><td>" . $row->cidade . "</td><td>" . $row->n . "</td><td>" . $row->estado . "</td><td>" . $row->n . "</td><td>" . $row->profissao . "</td><td>" . $row->n . "</td><td>" . $row->referencia . "</td><td>" . $row->n . "</td><td>" . $row->trabalho . "</td><td>" . $row->n . "</td><td>" . $row->idregiao . " - " . $regiao . "</td><td>" . $row->n . "</td></tr>";		
		}
		$html = $html . "</table>";		
		
		header("Content-type: application/vnd.ms-excel");  // Determina que o arquivo é uma planilha do Excel		
		header("Content-type: application/force-download");  // Força o download do arquivo 
		header("Content-Disposition: attachment; filename=relatorio-completo.xls"); // Seta o nome do arquivo		
		header("Pragma: no-cache");		
		echo $html;
	}
	
	function gerarRelatorioAniversariantesAction($dataInicio, $dataFim)
	{
		$this->load->model('Pessoa_model', 'pessoa');
		$arrayPessoas = $this->pessoa->buscarAniversariantes();		
		
		$html = "<table width='80%'><tr><th>Nome</th><th>E-mail </th><th>Telefone</th><th>Celular</th><th>Data de Nascimento</th></tr>";
		foreach ($arrayPessoas as $row) {
			$html = $html . "<tr><td>" . mb_convert_encoding($row->nome,'utf-16','utf-8') . "</td><td>" . $row->email . "</td><td>" . $row->telefone . "</td><td>" . $row->celular . "</td><td>" . implode("/",array_reverse(explode("-",$row->nascimento))) . "</td></tr>";		
		}
		$html = $html . "</table>";		
		
		header("Content-type: application/vnd.ms-excel");  // Determina que o arquivo é uma planilha do Excel		
		header("Content-type: application/force-download");  // Força o download do arquivo 
		header("Content-Disposition: attachment; filename=aniversariantes.xls"); // Seta o nome do arquivo		
		header("Pragma: no-cache");		
		echo $html;
	}
	
	function gerarRelatorioSemDataNascAction()
	{
    	$this->load->model('Pessoa_model', 'pessoa');
		$arrayPessoas = $this->pessoa->buscarPessoasSemDataNasc();
		
		$html = "<table width='80%'><tr><th>Nome</th><th>E-mail </th><th>Telefone</th></tr>";
		foreach ($arrayPessoas as $row) {
			$html = $html . "<tr><td>" . mb_convert_encoding($row->nome,'utf-16','utf-8') . "</td><td>" . $row->email . "</td><td>" . $row->telefone . "</td></tr>";		
		}
		$html = $html . "</table>";		
		
		header("Content-type: application/vnd.ms-excel");  // Determina que o arquivo é uma planilha do Excel		
		header("Content-type: application/force-download");  // Força o download do arquivo 
		header("Content-Disposition: attachment; filename=semaniversario.xls"); // Seta o nome do arquivo		
		header("Pragma: no-cache");		
		echo $html;
	}
	
	function buscarPessoasPorAtendimento($id)
	{
    	$this->load->model('Pessoa_model', 'pessoa');
		$arrayPessoas = $this->pessoa->buscarPessoasPorAtendimento($id);
		
		$html = "<table width='80%'><tr><th>Nome</th><th>E-mail </th><th>Telefone</th><th>Endereço</th></tr>";
		foreach ($arrayPessoas as $row) {
			$html = $html . "<tr><td>" . mb_convert_encoding($row->nome,'utf-16','utf-8') . "</td><td>" . $row->email . "</td><td>" . $row->fixo . " " . $row->celular . " " . $row->outro . "</td><td>" . $row->cep . " -" . mb_convert_encoding($row->rua,'utf-16','utf-8') . ", " . $row->numero . " - " . mb_convert_encoding($row->bairro,'utf-16','utf-8') . "  " . $row->complemento . " - " . $row->cidade . "/" . $row->estado . "</td></tr>";		
		}
		$html = $html . "</table>";		
		
		header("Content-type: application/vnd.ms-excel");  // Determina que o arquivo é uma planilha do Excel		
		header("Content-type: application/force-download");  // Força o download do arquivo 
		header("Content-Disposition: attachment; filename=pessoasporatendimento.xls"); // Seta o nome do arquivo		
		header("Pragma: no-cache");		
		echo $html;
	}
	
	function gerarRelatorioEmailAction()
	{    	
		$this->load->model('Pessoa_model', 'pessoa');
		$arrayPessoas = $this->pessoa->buscarEmails();
		
		$html = "<table width='80%'><tr><th>E-mail </th></tr>";
		foreach ($arrayPessoas as $row) {
			$html = $html . "<tr><td>" . $row->email . "</td></tr>";		
		}
		$html = $html . "</table>";		
		
		header("Content-type: application/vnd.ms-excel");  // Determina que o arquivo é uma planilha do Excel		
		header("Content-type: application/force-download");  // Força o download do arquivo 
		header("Content-Disposition: attachment; filename=e-mails.xls"); // Seta o nome do arquivo		
		header("Pragma: no-cache");		
		echo $html;

	}
	
	function gerarRelatorioProfessoresEscolas()
	{    	
		$this->load->model('Pessoa_model', 'pessoa');
		$arrayPessoas = $this->pessoa->buscarProfessoresEscolas();
		
		$html = "<table width='80%'><tr><th>Nome</th><th>E-mail</th><th>Telefones</th><th>Endereço</th><th>Trabalho</th><th>Profissão</th></tr>";
		foreach ($arrayPessoas as $row) {
			$html = $html . "<tr><td>" . mb_convert_encoding($row->nome,'utf-16','utf-8') . "</td><td>" . $row->email . "</td><td>" . $row->fixo . " " . $row->celular . " " . $row->outro . "</td><td>" . $row->cep . " -" . mb_convert_encoding($row->rua,'utf-16','utf-8') . ", " . $row->numero . " - " . mb_convert_encoding($row->bairro,'utf-16','utf-8') . "  " . $row->complemento . " - " . $row->cidade . "/" . $row->estado . "</td><td>" . mb_convert_encoding($row->trabalho,'utf-16','utf-8') . "</td><td>" . mb_convert_encoding($row->profissao,'utf-16','utf-8') . "</td></tr>";		
		}
		$html = $html . "</table>";		
		
		header("Content-type: application/vnd.ms-excel");  // Determina que o arquivo é uma planilha do Excel		
		header("Content-type: application/force-download");  // Força o download do arquivo 
		header("Content-Disposition: attachment; filename=professores-escolas.xls"); // Seta o nome do arquivo		
		header("Pragma: no-cache");		
		echo $html;

	}
    
}

?>