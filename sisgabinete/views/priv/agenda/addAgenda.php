<?php header("Content-Type: text/html; charset=utf-8", true); ?>	
<?
$this->load->view('priv/_inc/superior');
?>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Avisos/Compromissos</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><a href="<?= base_url() ?>principal/arearestrita">Principal</a> &raquo; <a href="<?= BASE_URL(); ?>agendaController/">Avisos/Compromissos</a> &raquo; Cadastrar</div>
			</div>
			<form method="post" action="addAgenda">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label>Título</label><br>
							<input type="text" name="titulo" id="titulo" class="form-control" />
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<label>Data</label><br>
							<input type="text" name="data" id="data" class="data form-control" />
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<label>Hora</label><br>
							<input type="text" name="hora" id="hora" class="form-control hora" />
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<label>Local</label><br>
							<input type="text" name="local" id="local" class="form-control" />
						</div>
					</div>
					<div class="col-lg-8">
						<div class="form-group">
							<label>Contato</label><br>
							<input type="text" name="contato" id="contato" class="form-control" />
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<label>Telefone</label><br>
							<input type="text" name="telefone" id="telefone" class="telefone form-control" />
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label>Observações</label><br>
							<textarea name="observacoes" id="observacoes" class="form-control"></textarea>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<input type="button" value="Voltar" class="btn btn-default" onclick="location.href='<?= base_url() ?>agendaController'"  />
							<input type="submit" class="btn btn-success" name="btSalvar" value="Salvar" />
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?
$this->load->view('priv/_inc/inferior');
?>
