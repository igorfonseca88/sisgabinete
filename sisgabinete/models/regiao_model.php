<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?php
class Regiao_model extends CI_Model {
    
    function getAll() {
        $query = $this->db->query("select * from tb_regiao order by titulo asc");
        return $query->result();
    }
	
    function add_record($options = array()) {
        $this->db->insert('tb_regiao', $options);
        return $this->db->insert_id();
    }
    
    function update($id, $options = array()) {
        $this->db->where('id', $id);
        $this->db->update('tb_regiao',$options);
        return $this->db->affected_rows();
    }
    
    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('tb_regiao',$options);
        return $this->db->affected_rows();
    }
    
    function buscarPorId($id) {
        $this->db->where('id', $id);
        $query = $this->db->get("tb_regiao");
        return $query->result();
    }
}
?>