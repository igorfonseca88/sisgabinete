<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?
$this->load->view('priv/_inc/superior');
?>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Regiões</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><a href="<?= base_url() ?>principal/arearestrita">Principal</a> &raquo; <a href="<?= BASE_URL(); ?>regiaoController/">Regiões</a> &raquo; Editar</div>
			</div>
				
			<?= $sucesso != "" ? '<div class="alert alert-success"> ' . $sucesso . ' </div>' : "" ?>
			<?= $erro != "" ? '<div class="alert alert-danger"> ' . $erro . ' </div>' : "" ?>

			<? foreach ($regiao as $row) { ?>
			<form method="post" action="<?= base_url() ?>regiaoController/editRegiao">
				<input type="hidden" name="id" id="id" value="<?= $row->id ?>"/>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label>Título</label><br>
							<input type="text" name="titulo" id="titulo" value="<?=$row->titulo?>" class="form-control" />
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<input type="button" value="Voltar" class="btn btn-default" onclick="location.href='<?= base_url() ?>regiaoController'"  />
							<input type="submit" class="btn btn-success" name="btSalvar" value="Salvar" />
						</div>
					</div>
				</div>
			</form>
		<? } ?>
	</div>
</div>
<?
$this->load->view('priv/_inc/inferior');
?>
