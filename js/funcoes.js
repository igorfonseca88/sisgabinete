﻿$(document).ready(function() {
	/*Máscaras*/
	$('.telefone').focusout(function(){
		var phone, element;
		element = $(this);
		element.unmask();
		phone = element.val().replace(/\D/g, '');
		if(phone.length > 10) {
			element.mask("(99) 99999-999?9");
		} else {
			element.mask("(99) 9999-9999?9");
		}
	}).trigger('focusout');
	$(".cpf").mask("999.999.999-99");
	$(".cep").mask("99999-999");
	$(".data").mask("99/99/9999");
	$(".hora").mask("99:99");
	
	$("#dataTables-example").dataTable();
	
    $(".numeros").keyup(function() {
		var valor = $(this).val().replace(/[^0-9\,\.]+/g,'');
		$(this).val(valor);
     });

	$("#txtDataIni").datepicker({
		dateFormat: "dd/mm/yy",
		dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
		dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
		dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		monthNamesShort: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		buttonText: "Select date",
		showOn: 'button',
		buttonImage: 'img/calendar.gif',
		buttonImageOnly: true
	});
	
	$("#txtDataFim").datepicker({
		dateFormat: "dd/mm/yy",
		dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
		dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
		dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		monthNamesShort: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		buttonText: "Select date",
		showOn: 'button',
		buttonImage: 'img/calendar.gif',
		buttonImageOnly: true
	});

});
