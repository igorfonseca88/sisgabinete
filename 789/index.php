<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?php
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>

        <title>jQuery Autocomplete Plugin</title>
        <link rel="stylesheet" type="text/css" href="js/jquery.autocomplete.css" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
		<script type="text/javascript" src="http://www.eduardoromero.com.br/sistema/js/maskedinput.js"></script> 
	    <script type="text/javascript" src="http://www.eduardoromero.com.br/sistema/js/bootstrap.min.js"></script>
	    <script type="text/javascript" src="http://www.eduardoromero.com.br/sistema/js/sb-admin.js"></script>
		<script type="text/javascript" src="http://www.eduardoromero.com.br/sistema/system/ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="http://www.eduardoromero.com.br/sistema/system/ckeditor/config.js"></script>
		<script type="text/javascript" src="http://www.eduardoromero.com.br/sistema/js/plugins/dataTables/jquery.dataTables.js"></script>
		<script type="text/javascript" src="http://www.eduardoromero.com.br/sistema/js/plugins/dataTables/dataTables.bootstrap.js"></script>
        <script type='text/javascript' src="http://www.eduardoromero.com.br/sistema/js/jquery.autocomplete.js"></script>
		<script type="text/javascript" src="http://www.eduardoromero.com.br/sistema/js/jquery.fancybox.js"></script>
		<script type="text/javascript" src="http://www.eduardoromero.com.br/sistema/js/funcoes.js"></script>
        <script type="text/javascript">
            $().ready(function() {
                $("#course").autocomplete("autoComplete.php", {
                    width: 260,
                    matchContains: true,
                    //mustMatch: true,
                    //minChars: 0,
                    //multiple: true,
                    //highlight: false,
                    //multipleSeparator: ",",
                    selectFirst: true
                });
            });
        </script>
    </head>
    <body>
        <h2>Autocomplete usando jQuery, Ajax, PHP</h2>
        <div id="content">
            <form autocomplete="off">
                <p>
                    Digite um nome:
                    <input type="text" name="course" id="course" />
                </p>

            </form>
        </div>
</body>
</html>
