<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?php

class RegiaoController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }
	
	function index()
	{
		$this->load->model('Regiao_model', 'regiao');
		$data["regiao"] = $this->regiao->getAll();
		
		$this->load->vars($data);
		$this->load->view("priv/regiao/listRegiao");
	}
	
	function novaRegiaoAction()
	{
		$this->load->view("priv/regiao/addRegiao");
	}
	
	function addRegiao()
	{
		$this->load->model("Regiao_model", "regiao");		
		$insert = array(
			"titulo" => $this->input->post("titulo")
		);
		
		if ($this->regiao->add_record($insert) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["error"] = "Erro ao salvar.";
		}
		
		$data["regiao"] = $this->regiao->getAll();
		$this->load->vars($data);
		$this->load->view("priv/regiao/listRegiao");
	}
	
	function deleteRegiao($id)
	{
		$this->load->model("Regiao_model", "regiao");		

		if ($this->regiao->delete($id) > 0) {
			$data["sucesso"] = "Excluído com sucesso.";
		} else {
			$data["error"] = "Erro ao excluir.";
		}
		
		$data["regiao"] = $this->regiao->getAll();
		$this->load->vars($data);
		$this->load->view("priv/regiao/listRegiao");
	}

	function editRegiao() {
		$this->load->model("Regiao_model", "regiao");
		$id = $this->input->post("id");
		
		$update = array(
			"titulo" => $this->input->post("titulo")
		);
		
		if ($this->regiao->update($id,$update) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["erro"] = "Erro ao salvar.";
		}
		
		$data["regiao"] = $this->regiao->getAll();
		$this->load->vars($data);
		$this->load->view("priv/regiao/listRegiao");
    }
    
    function editarRegiaoAction($id, $mensagem = array()) {   
        $this->load->model('Regiao_model', 'regiao');
        $data["regiao"] = $this->regiao->buscarPorId($id);
		
        $data["sucesso"] = $mensagem["sucesso"];
        $data["error"] = $mensagem["error"];
			   
        $this->load->vars($data);
        $this->load->view("priv/regiao/editRegiao");
    } 
}

?>