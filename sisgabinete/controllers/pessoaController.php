<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?php

class PessoaController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }
	
	function index()
	{
		
		if($this->session->userdata("idEmpresa") == null || $this->session->userdata("idEmpresa") == ""){
			redirect("/");
		}
		
		$this->load->model('Pessoa_model', 'pessoa');		
		$data["pessoa"] = $this->pessoa->buscarPessoas($this->session->userdata("idEmpresa"));
		
		$this->load->vars($data);
		$this->load->view("priv/pessoa/listPessoa");
	}
	
	function buscaAction()
	{
		$this->load->model('Pessoa_model', 'pessoa');
		$data["pessoa"] = $this->pessoa->buscarPessoasPorNome($this->input->post("nome"),$this->session->userdata("idEmpresa"));
		
		$this->load->vars($data);
		$this->load->view("priv/pessoa/listPessoa");
	}
	
	function novaPessoaAction()
	{		
        $this->load->model('Regiao_model', 'regiao');
        $data["regiao"] = $this->regiao->getAll();
		
		$this->load->vars($data);
		$this->load->view("priv/pessoa/addPessoa");
	}
	
	function addPessoa()
	{
		$this->load->model("Pessoa_model", "pessoa");		
		$insert = array(
			"dataCadastro" => implode("-",array_reverse(explode("/",$this->input->post("dataCadastro")))),
			"nome" => $this->input->post("nome"),
			"email" => $this->input->post("email"),
			//"sexo" => $this->input->post("sexo"),
			"nascimento" => implode("-",array_reverse(explode("/",$this->input->post("nascimento")))),
			"fixo" => $this->input->post("fixo"),
			"celular" => $this->input->post("celular"),
			//"outro" => $this->input->post("outro"),
			"cep" => $this->input->post("cep"),
			"endereco" => $this->input->post("rua"),
			"numero" => $this->input->post("numero"),
			"bairro" => $this->input->post("bairro"),
			"complemento" => $this->input->post("complemento"),
			"cidade" => $this->input->post("cidade"),
			"estado" => $this->input->post("estado"),
			"profissao" => $this->input->post("profissao"),
			"referencia" => $this->input->post("referencia"),
			"trabalho" => $this->input->post("trabalho"),
			"idregiao" => $this->input->post("idregiao"),
			"idUsuario" => $this->session->userdata("idUsuario"),
			"idEmpresa" => $this->session->userdata("idEmpresa")
		);
		$id = $this->pessoa->add_record($insert);
		if ( $id > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["error"] = "Erro ao salvar.";
		}
		$this->editarPessoaAction($id, $data);
	}
	
	function deletePessoa($id)
	{
		$this->load->model("Pessoa_model", "pessoa");		

		if ($this->pessoa->delete($id) > 0) {
			$data["sucesso"] = "Excluído com sucesso.";
		} else {
			$data["error"] = "Erro ao excluir.";
		}
		
		$data["pessoa"] = $this->pessoa->buscarPessoas();
		$this->load->vars($data);
		$this->load->view("priv/pessoa/listPessoa");
	}
	
	function editPessoa() {
		$this->load->model("Pessoa_model", "pessoa");
		$id = $this->input->post("id");
		
		$update = array(
			"dataCadastro" => implode("-",array_reverse(explode("/",$this->input->post("dataCadastro")))),
			"nome" => $this->input->post("nome"),
			"email" => $this->input->post("email"),
			"sexo" => $this->input->post("sexo"),
			"nascimento" => implode("-",array_reverse(explode("/",$this->input->post("nascimento")))),
			"fixo" => $this->input->post("fixo"),
			"celular" => $this->input->post("celular"),
			"outro" => $this->input->post("outro"),
			"cep" => $this->input->post("cep"),
			"endereco" => $this->input->post("rua"),
			"numero" => $this->input->post("numero"),
			"bairro" => $this->input->post("bairro"),
			"complemento" => $this->input->post("complemento"),
			"cidade" => $this->input->post("cidade"),
			"estado" => $this->input->post("estado"),
			"profissao" => $this->input->post("profissao"),
			"referencia" => $this->input->post("referencia"),
			"trabalho" => $this->input->post("trabalho"),
			"idregiao" => $this->input->post("idregiao")
		);

		if ($this->pessoa->update($id,$update) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["erro"] = "Erro ao salvar.";
		}
		
		$this->editarPessoaAction($id, $data);
    }
    
    function editarPessoaAction($id, $mensagem = array()) {   

		// TODO mudar para buscar por empresa
        $this->load->model('Pessoa_model', 'pessoa');
        $data["pessoa"] = $this->pessoa->buscarPorId($id,$this->session->userdata("idEmpresa"));
		
        $this->load->model('Regiao_model', 'regiao');
        $data["regiao"] = $this->regiao->getAll();
		
		$this->load->model('Assunto_model', 'assunto');
        $data["assunto"] = $this->assunto->getAll();
		
		$this->load->model('Atendimento_model', 'atendimento');
        $data["atendimentos"] = $this->atendimento->buscarAtendimentosPorPessoa($id);
		
		$this->load->model('Usuario_model', 'user');
        $data["usuarios"] = $this->user->getAll();
		
        $data["sucesso"] = $mensagem["sucesso"];
        $data["error"] = $mensagem["error"];
		
		// limpa a edição do atendimento
		$data["editAtendimento"] = "";
			   
        $this->load->vars($data);
        $this->load->view("priv/pessoa/editPessoa");
    }
	
	function novoAtendimento(){
		$this->load->model("Atendimento_model", "atendimento");		
		
		$insert = array(
			"idpessoa" => $this->input->post("idpessoa"),
			"data" => implode("-",array_reverse(explode("/",$this->input->post("data")))),
			"idAssunto" => $this->input->post("idAssunto"),
			"ocorrencia" => $this->input->post("ocorrencia"),
			"situacao" => $this->input->post("situacao"),
			"local" => $this->input->post("local"), 
			"idUsuario"=> $this->input->post("idUsuario")
		);
		
		if ($this->atendimento->add_record($insert) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["error"] = "Erro ao salvar.";
		}
		
		$this->editarPessoaAction($this->input->post("idpessoa"), $data);
	}
	
	function editarAtendimentoAction($id, $idPessoa){
		$this->load->model("Atendimento_model", "atendimento");	
		$data["atendimentoEdit"] = $this->atendimento->buscarPorId($id);
		$data["editAtendimento"] = "S";
		
		$this->load->model('Pessoa_model', 'pessoa');
        $data["pessoa"] = $this->pessoa->buscarPorId($idPessoa);
		
        $this->load->model('Regiao_model', 'regiao');
        $data["regiao"] = $this->regiao->getAll();
		
		$this->load->model('Assunto_model', 'assunto');
        $data["assunto"] = $this->assunto->getAll();
		
		$this->load->model('Usuario_model', 'user');
        $data["usuarios"] = $this->user->getAll();
		
		$this->load->model('Atendimento_model', 'atendimento');
        $data["atendimentos"] = $this->atendimento->buscarAtendimentosPorPessoa($idPessoa);		
			   
        $this->load->vars($data);
        $this->load->view("priv/pessoa/editPessoa");
		
	}
	
	function editarAtendimento(){
		$this->load->model("Atendimento_model", "atendimento");		
		$id = $this->input->post("idAtendimento");
		$update = array(			
			"data" => implode("-",array_reverse(explode("/",$this->input->post("data")))),
			"idAssunto" => $this->input->post("idAssunto"),
			"ocorrencia" => $this->input->post("ocorrencia"),
			"situacao" => $this->input->post("situacao"),
			"local" => $this->input->post("local")
		);
		
		if ($this->atendimento->update($id,$update) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["erro"] = "Erro ao salvar.";
		}
		
		$this->editarPessoaAction($this->input->post("idpessoa"), $data);
	}
	
	function deleteAtendimento($id){
		$this->load->model("Atendimento_model", "atendimento");	
		$data["atendimento"] = $this->atendimento->buscarPorId($id);
				
		if ($this->atendimento->delete($id) > 0) {
			$data["sucesso"] = "Atendimento excluído com sucesso.";
		} else {
			$data["error"] = "Erro ao excluir atendimento.";
		}		
		$this->editarPessoaAction($data["atendimento"][0]->idpessoa, $data);
	}
	
	function exportar() {
		$this->load->model('Pessoa_model', 'pessoa');
		$aniversariantes = $this->pessoa->buscarAniversariantes();
		
		$html = "<table><tr> <th> Aniversário </th> <th> Nome </th> <th> Telefones </th> <th> Último atendimento </th></tr>";
		foreach ($aniversariantes as $row) {
			$html = $html . "<tr><td>" . implode("/",array_reverse(explode("-",$row->nascimento))) . "</td><td>" . $row->nome . "</td><td>" . $row->celular . " - " . $row->fixo . "</td><td>" . $row->atendimento . "</td></tr>";		
		}
		$html = $html . "</table>";		
		
		header("Content-type: application/vnd.ms-excel");
		header("Content-type: application/force-download");
		header("Content-Disposition: attachment; filename=aniversarios.xls");
		header("Pragma: no-cache");		
		echo $html;
	}
	
	function buscarNomes()
	{
		$q = strtolower($_GET["q"]);
		if (!$q) return;
		
		$this->load->model("Pessoa_model", "pessoa");		
		$data = $this->pessoa->buscarNomes($q);
		foreach ($data as $d) {
			$cname = $d->nome;
			echo "$cname\n";
		}
	}
	
	function buscarProfissoes()
	{
		$q = strtolower($_GET["q"]);
		if (!$q) return;
		
		$this->load->model("Pessoa_model", "pessoa");		
		$data = $this->pessoa->buscarProfissoes($q);
		foreach ($data as $d) {
			$cprofissao = $d->profissao;
			echo "$cprofissao\n";
		}
	}
	
	function buscarTrabalho()
	{
		$q = strtolower($_GET["q"]);
		if (!$q) return;
		
		$this->load->model("Pessoa_model", "pessoa");		
		$data = $this->pessoa->buscarTrabalho($q);
		foreach ($data as $d) {
			$ctrabalho = $d->trabalho;
			echo "$ctrabalho\n";
		}
	}
	
	function buscarEmail()
	{
		$q = strtolower($_GET["q"]);
		if (!$q) return;
		
		$this->load->model("Pessoa_model", "pessoa");		
		$data = $this->pessoa->buscarEmail($q);
		foreach ($data as $d) {
			$cemail = $d->email;
			echo "$cemail\n";
		}
	}
	
	function buscarCidade()
	{
		$q = strtolower($_GET["q"]);
		if (!$q) return;
		
		$this->load->model("Pessoa_model", "pessoa");		
		$data = $this->pessoa->buscarCidade($q);
		foreach ($data as $d) {
			$nome = $d->nome;
			echo "$nome\n";
		}
	}	
}

?>