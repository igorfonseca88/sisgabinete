<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?
$this->load->view('priv/_inc/superior');
?>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Dados do gabinete</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><a href="<?= base_url() ?>principal/arearestrita">Principal</a> &raquo; Dados do gabinete</div>
			</div>
			<?= $sucesso != "" ? '<div class="alert alert-success"> ' . $sucesso . ' </div>' : "" ?>
			<?= $erro != "" ? '<div class="alert alert-danger"> ' . $erro . ' </div>' : "" ?>

			<? foreach ($empresa as $row) { ?>
			<form method="post" action="<?= BASE_URL(); ?>empresaController/editEmpresa">			
				<input type="hidden" name="id" id="id" value="<?= $row->id ?>"/>
				<div class="form-group">
					<label>Nome</label><br />
					<input type="text" name="razaoSocial" id="razaoSocial" value="<?= $row->razaoSocial ?>" class="form-control" />
				</div>
				<div class="form-group">
					<label>E-mail para contato</label><br />
					<input type="text" name="email" id="email" value="<?= $row->email ?>" class="form-control" />
				</div>
				<div class="form-group">
					<label>Telefone</label><br />
					<input type="text" name="telefone" id="telefone" value="<?= $row->telefone ?>" class="form-control telefone" />
				</div>
				<div class="form-group">
					<label>Esfera</label><br />
					
					<select name="esfera" id="esfera" class="form-control">
						<option value="">Selecione</option>
						<option value="Municipal">Municipal</option>
						<option value="Estadual">Estadual</option>
						<option value="Federal">Federal</option>
					</select>
				</div>
				
				<div class="form-group">
					<label>Estado</label><br />
					<select name="estado" id="estado" class="form-control">
						<option value="">Selecione</option>
						<? foreach ($estados as $estado) { ?>
							<option <?= $empresa[0]->estado == $estado->id ? "selected" : "" ?>
								value="<?= $estado->id ?>"><?= $estado->nome ?></option>
						<? } ?>
					</select>
				</div>
				
				<div class="form-group">
					<label>Cidade</label><br />
					<input type="text" name="cidade" id="cidade" value="<?= $row->cidade ?>" class="form-control" />
				</div>
				<!--<div class="form-group">
					<label>Servidor para envio de e-mail: SMTP</label><br />
					<input type="text" name="smtp" id="smtp" value="<?//= $row->smtp ?>" class="form-control" />
				</div>
				<div class="form-group">
					<label>Servidor para envio de e-mail: Porta</label><br />
					<input type="text" name="porta" id="porta" value="<?//= $row->porta ?>" class="form-control" />
				</div>
				<div class="form-group">
					<label>Servidor para envio de e-mail: E-mail</label><br />
					<input type="text" name="usuario" id="usuario" value="<?//= $row->usuario ?>" class="form-control" />
				</div>
				<div class="form-group">
					<label>Servidor para envio de e-mail: Senha</label><br />
					<input type="text" name="senha" id="senha" value="<?//= $row->senha ?>" class="form-control" />
				</div> -->
				<div class="form-group">
					<input type="button" value="Voltar" class="btn btn-default" onClick="location.href='<?= base_url() ?>principal/arearestrita'" />
					<input type="submit" class="btn btn-success" name="btSalvarEmpresa" value="Salvar Empresa" />
				</div>
			</form>
		<? } ?>
	</div>
</div>

<?
$this->load->view('priv/_inc/inferior');
?>
