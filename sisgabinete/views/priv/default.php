<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<? $this->load->view('priv/_inc/superior');?>

<div id="page-wrapper">
	<div class="row">
	
		<div class="col-lg-12"> 
			<!--Título-->
			<h1 class="page-header">Dashboard</h1>
			
			<!--Acesso rápido
			<a class="btn btn-success" onclick="$('#modal').modal('show')">Novo atendimento rápido</a>--> 
		</div>

	</div>
	<div class="row">

		<!--Painel-->
		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading"><i class="fa fa-signal fa-fw"></i> Pessoas cadastradas</div>
				<div class="panel-body">
					<div id="morris-area-chart"></div>
				</div>
			</div>
		</div>
		
		<!--Painel-->
		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading"><i class="fa fa-signal fa-fw"></i> Atendimentos realizados</div>
				<div class="panel-body">
					<div id="morris-donut-chart"></div>
				</div>
			</div>
		</div>
	</div>
		<!--Painel-->
		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading"> <i class="fa fa-star fa-fw"></i> Aniversariantes da semana (Total: <?=count($aniversariantes)?>) <a href="<?=base_url()?>pessoaController/exportar" style="float:right">Exportar em EXCEL</a></div>
				<div class="panel-body">
					<div style="height:400px;overflow:auto;display:block">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th> Aniversário </th>
									<th width="300"> Nome </th>
									<th width="120"> Telefones </th>
									<th> Último atendimento </th>
									<th width="150"> Mais informações </th>
								</tr>
							</thead>
							<tbody>
								<? foreach ($aniversariantes as $row) { ?>
								<? $data = implode("/",array_reverse(explode("-",$row->nascimento))); ?>
								<tr>
									<td><?= substr($data,0,-5) ?></td>
									<td><?=$row->nome?></td>
									<td><?=$row->celular <> "" ? $row->celular : $row->fixo ?> </td>
									<td> <?= $row->atendimento <> "" ? $row->atendimento : "-" ?></td>
									<td> <a href="<?=base_url()?>pessoaController/editarPessoaAction/<?=$row->id?>" target="_blank">Ver mais...</a> </td>
								</tr>
								<? } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<!--Painel-->
		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading"> <i class="fa fa-star fa-fw"></i> Atividades recentes (Total: <?=count($aniversariantes)?>) <a href="<?=base_url()?>pessoaController/exportar" style="float:right">Exportar em EXCEL</a></div>
				<div class="panel-body">
					<div style="height:400px;overflow:auto;display:block">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
								
									<th width="300"> Nome </th>
									<th width="120"> Telefones </th>
									<th> Último atendimento </th>
									<th width="150"> Mais informações </th>
								</tr>
							</thead>
							<tbody>
								<? foreach ($ultimasAtividades as $row) { ?>
								
								<tr>
									
									<td><?=$row->nome?></td>
									<td><?=$row->celular <> "" ? $row->celular : $row->fixo ?> </td>
									<td> <?= $row->atendimento <> "" ? $row->atendimento : "-" ?></td>
									<td> <a href="<?=base_url()?>pessoaController/editarPessoaAction/<?=$row->id?>" target="_blank">Ver mais...</a> </td>
								</tr>
								<? } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
		
</div>



<? $this->load->view('priv/_inc/inferior'); ?>
