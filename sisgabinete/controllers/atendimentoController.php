<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?php

class AtendimentoController extends CI_Controller {

    function __construct() {
        parent::__construct();
		if($this->session->userdata("idEmpresa") == null || $this->session->userdata("idEmpresa") == ""){
			redirect("/");
		}
    }
	
	function index()
	{
		$this->load->model('Atendimento_model', 'atendimento');
		$data["atendimento"] = $this->atendimento->buscarAtendimentosPorEmpresa($this->session->userdata("idEmpresa"));
		
		$this->load->vars($data);
		$this->load->view("priv/atendimento/listAtendimento");
	}
	
	function novoAtendimentoAction()
	{
		$this->load->view("priv/atendimento/addAtendimento");
	}
	
	function addAtendimento()
	{
		$this->load->model("Atendimento_model", "atendimento");		
		$insert = array(
			"idpessoa" => $this->input->post("idpessoa"),
			"data" => implode("-",array_reverse(explode("/",$this->input->post("data")))),
			"titulo" => $this->input->post("titulo"),
			"ocorrencia" => $this->input->post("ocorrencia"),
			"situacao" => $this->input->post("situacao"),
			"idEmpresa" => $this->session->userdata("idEmpresa")
		);
		
		if ($this->atendimento->add_record($insert) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["error"] = "Erro ao salvar.";
		}
		$this->editarAtendimentoAction($id, $data);
	}
	
	function deleteAtendimento($id)
	{
		$this->load->model("Atendimento_model", "atendimento");		

		if ($this->atendimento->delete($id) > 0) {
			$data["sucesso"] = "Excluído com sucesso.";
		} else {
			$data["error"] = "Erro ao excluir.";
		}
		
		$data["atendimento"] = $this->atendimento->buscarAtendimentosPorEmpresa($this->session->userdata("idEmpresa"));
		$this->load->vars($data);
		$this->load->view("priv/atendimento/listAtendimento");
	}

	function editAtendimento() {
		$this->load->model("Atendimento_model", "atendimento");
		$id = $this->input->post("id");
		
		$update = array(
			"idpessoa" => $this->input->post("idpessoa"),
			"data" => implode("-",array_reverse(explode("/",$this->input->post("data")))),
			"titulo" => $this->input->post("titulo"),
			"ocorrencia" => $this->input->post("ocorrencia"),
			"situacao" => $this->input->post("situacao")
		);
		
		if ($this->atendimento->update($id,$update) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["erro"] = "Erro ao salvar.";
		}
		
		$this->editarAtendimentoAction($id, $data);
    }
    
    function editarAtendimentoAction($id, $mensagem = array()) {   
        $this->load->model('Atendimento_model', 'atendimento');
        $data["atendimento"] = $this->atendimento->buscarPorId($id);
		
        $data["sucesso"] = $mensagem["sucesso"];
        $data["error"] = $mensagem["error"];
			   
        $this->load->vars($data);
        $this->load->view("priv/atendimento/editAtendimento");
    } 
}

?>