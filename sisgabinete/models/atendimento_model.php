<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?php
class Atendimento_model extends CI_Model {
    
    function getAll() {
        $query = $this->db->query("select * from tb_atendimento order by titulo asc");
        return $query->result();
    }
	
	function buscarAtendimentosPorEmpresa($idEmpresa) {
        $sql = "select * from tb_atendimento where idEmpresa = ? order by titulo asc";
		$query = $this->db->query($sql, array($idEmpresa));
        return $query->result();
    }
	
    function add_record($options = array()) {
        $this->db->insert('tb_atendimento', $options);
        return $this->db->insert_id();
    }
    
    function update($id, $options = array()) {
        $this->db->where('id', $id);
        $this->db->update('tb_atendimento',$options);
        return $this->db->affected_rows();
    }
    
    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('tb_atendimento',$options);
        return $this->db->affected_rows();
    }
    
    function buscarPorId($id) {
        $this->db->where('id', $id);
        $query = $this->db->get("tb_atendimento");
        return $query->result();
    }
	
	function buscarAtendimentosPorPessoa($idPessoa) {
        $query = $this->db->query("select at.*, a.assunto, local from tb_atendimento at left join tb_assunto a ON at.idAssunto = a.id where at.idPessoa = $idPessoa order by at.data desc");
        return $query->result();
    }
}
?>