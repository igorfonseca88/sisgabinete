<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?$this->load->view('priv/_inc/superior');?>

<script>
	function emitirAtendimento() {
		if ($("#atendimento").val() == 0) {
			alert ("Escolha um tipo de atendimento.");
		} else {
			location.href = "<?=base_url()?>relatorioController/buscarPessoasPorAtendimento/" + $("#atendimento").val();
		}
	}
</script>


<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Relatórios</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><a href="<?= base_url() ?>principal/arearestrita">Principal</a> &raquo; Relatórios </div>
			</div>
				
			<?= $sucesso != "" ? '<div class="alert alert-success"> ' . $sucesso . ' </div>' : "" ?>
			<?= $erro != "" ? '<div class="alert alert-danger"> ' . $erro . ' </div>' : "" ?>
			
			<table class="table table-striped table-bordered table-hover" id="dataTables-example">
				<thead>
					<th>Relatório</th>
					
					<!--<th>Tipo</th> -->
					<th width="120" align="center">Ações</th>
				</thead>
				
				<tr>
					<td> Listagem de pessoas - dados simples</td>					 
					<td align="center"><a href="<?=base_url()?>relatorioController/gerarRelatorioAction/">Emitir</a></td>
				</tr>
				
				<tr>
					<td>Listagem de pessoas - dados completos</td>					
					<td align="center"><a href="<?=base_url()?>relatorioController/gerarRelatorioCompleto/">Emitir</a></td>
				</tr>
				
				<tr>
					<td> Listagem de e-mails </td>					 
					<td align="center"><a href="<?=base_url()?>relatorioController/gerarRelatorioEmailAction/">Emitir</a></td>
				 </tr>
				
				<tr>
					<td> Listagem de aniversários </td>					
					<td align="center"><a href="<?=base_url()?>relatorioController/gerarRelatorioAniversariantesAction/">Emitir</a></td>
				</tr>
				
				<tr>
					<td> Listagem de pessoas - sem data de nascimento </td>					
					<td align="center"><a href="<?=base_url()?>relatorioController/gerarRelatorioSemDataNascAction/">Emitir</a></td>
				</tr>
				
				<tr>
					<td> Listagem de professores e escolas</td>					
					<td align="center"><a href="<?=base_url()?>relatorioController/gerarRelatorioProfessoresEscolas/">Emitir</a></td>
				</tr>
				
				<tr>
					<td> 
						Listagem de pessoas - por atendimento 
						<select id="atendimento" name="atendimento" class="form-control" style="width:300px">
							<option value="0">Selecione</option>
							<? foreach ($assunto as $row) { ?>
							<option value="<?=$row->id?>"><?=$row->assunto?></option>
							<? } ?>
						</select>
					</td>					
					<td align="center"><a href="#" onclick="emitirAtendimento()">Emitir</a></td>
				</tr>
				
				
		   </table>
	    </div>
	</div>
</div>

<?$this->load->view('priv/_inc/inferior');?>