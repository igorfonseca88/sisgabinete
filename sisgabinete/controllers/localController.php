<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?php

class LocalController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }
	
	function index()
	{
		$this->load->model('Local_model', 'local');
		$data["local"] = $this->local->getAll();
		
		$this->load->vars($data);
		$this->load->view("priv/local/listLocal");
	}
	
	function novoLocalAction()
	{
		$this->load->view("priv/local/addLocal");
	}
	
	function addLocal()
	{
		$this->load->model("Local_model", "local");		
		$insert = array(
			"titulo" => $this->input->post("titulo")
		);
		
		if ($this->local->add_record($insert) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["error"] = "Erro ao salvar.";
		}
		
		$data["local"] = $this->local->getAll();
		$this->load->vars($data);
		$this->load->view("priv/local/listLocal");
	}
	
	function deleteLocal($id)
	{
		$this->load->model("Local_model", "local");		

		if ($this->local->delete($id) > 0) {
			$data["sucesso"] = "Excluído com sucesso.";
		} else {
			$data["error"] = "Erro ao excluir.";
		}
		
		$data["local"] = $this->local->getAll();
		$this->load->vars($data);
		$this->load->view("priv/local/listLocal");
	}

	function editLocal() {
		$this->load->model("Local_model", "local");
		$id = $this->input->post("id");
		
		$update = array(
			"titulo" => $this->input->post("titulo")
		);
		
		if ($this->local->update($id,$update) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["erro"] = "Erro ao salvar.";
		}
		
		$data["local"] = $this->local->getAll();
		$this->load->vars($data);
		$this->load->view("priv/local/listLocal");
    }
    
    function editarLocalAction($id, $mensagem = array()) {   
        $this->load->model('Local_model', 'local');
        $data["local"] = $this->local->buscarPorId($id);
		
        $data["sucesso"] = $mensagem["sucesso"];
        $data["error"] = $mensagem["error"];
			   
        $this->load->vars($data);
        $this->load->view("priv/local/editLocal");
    } 
}

?>