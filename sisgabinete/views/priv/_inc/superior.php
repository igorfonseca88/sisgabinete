<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?
if ($this->Usuario_model->logged() == FALSE) {
    redirect('/login/', 'refresh');
}

//Transforma títulos em URL amigáveis
function url($str) {
 $str = strtolower(utf8_decode($str)); $i=1;
    $str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
    $str = preg_replace("/([^a-z0-9])/",'-',utf8_encode($str));
    while($i>0) $str = str_replace('--','-',$str,$i);
    if (substr($str, -1) == '-') $str = substr($str, 0, -1);
    return $str;
}
?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8" />
		<meta name="robots" content="noindex,nofollow" />
		<title>SisGabinete | Gerenciador de Gabinete</title>
		<link href="<?=base_url()?>css/bootstrap.min.css" rel="stylesheet" />
		<link href="<?=base_url()?>css/font-awesome/css/font-awesome.css" rel="stylesheet" />
		<link href="<?=base_url()?>css/sb-admin.css" rel="stylesheet" />
		<link href="<?=base_url()?>css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
        <link href="<?=base_url()?>css/jquery.autocomplete.css"  rel="stylesheet" />
		<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/smoothness/jquery-ui.css" />
		<link rel="shortcut icon" href="<?=base_url()?>img/favico.ico" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/maskedinput.js"></script> 
	    <script type="text/javascript" src="<?=base_url()?>js/bootstrap.min.js"></script>
	    <script type="text/javascript" src="<?=base_url()?>js/sb-admin.js"></script>
		<script type="text/javascript" src="<?=base_url()?>system/ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="<?=base_url()?>system/ckeditor/config.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/plugins/dataTables/jquery.dataTables.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/plugins/dataTables/dataTables.bootstrap.js"></script>
        <script type='text/javascript' src="<?=base_url()?>js/jquery.autocomplete.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/jquery.fancybox.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/funcoes.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/plugins/morris/morris.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/plugins/morris/raphael-2.1.0.min.js"></script>
	</head>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
				<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?=base_url()?>principal/arearestrita">Olá <?= $this->session->userdata("nome") ?>!</a>
			</div>
			
            <ul class="nav navbar-top-links navbar-right">
				
				<!--Dropown-->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i> </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?=base_url()?>usuarioController/editarUsuarioAction/1"><i class="fa fa-user fa-fw"></i> Alterar senha</a></li>
                        <li class="divider"></li>
                        <li><a href="<?=base_url()?>login/login/logoff"><i class="fa fa-sign-out fa-fw"></i> Sair</a></li>
                    </ul>
                </li>
			</ul>
        </nav>

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
				
					<!--Logo-->
					<li class="sidebar-search">
						<a href="<?=base_url()?>principal/arearestrita"><img src="<?= base_url() ?>img/logo.png" width="100%" alt="Sistema" title="Sistema" /></a>
					</li>
					
					<!--Busca-->
					<form action="<?=base_url()?>pessoaController/buscaAction" method="post">
						<li class="sidebar-search">
							<div class="input-group custom-search-form">
								<input type="text" placeholder="Buscar pessoa por nome" name="nome" id="nome" value="<? echo $_POST["nome"]?>" class="form-control"/>
								<span class="input-group-btn">
								<button type="submit" class="btn btn-default">
								<i class="fa fa-search"></i></button></span>
							</div>
						</li>
					</form>
					
					<!--Menu-->
					<li> <a href="<?=base_url()?>principal/arearestrita"><i class="fa fa-dashboard fa-fw"></i> Principal </a> </li>
					<li> <a href="<?=base_url()?>pessoaController"><i class="fa fa-user fa-fw"></i> Pessoas </a> </li>
					<li> <a href="<?=base_url()?>pessoaController"><i class="fa fa-user fa-fw"></i> Empresas </a> </li>
					<li> <a href="<?=base_url()?>agendaController"><i class="fa fa-clock-o fa-fw"></i> Avisos/Compromissos </a> </li> 
					<li> <a href="<?=base_url()?>relatorioController"><i class="fa fa-bar-chart-o fa-fw"></i> Relatórios </a> </li>
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-folder-open fa-fw"></i> Cadastros básicos<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li> <a href="<?=base_url()?>assuntoController">Assuntos de atendimentos</a> </li>
							<!--<li> <a href="<?=base_url()?>localController">Locais de atendimento</a> </li>-->
							<li> <a href="<?=base_url()?>regiaoController">Regiões</a> </li>
							<!--<li> <a href="<?=base_url()?>grupoController">Grupos</a> </li>-->
							<!--<li> <a href="<?=base_url()?>secretariaController">Secretarias</a> </li>-->
							<!--<li> <a href="<?=base_url()?>profissaoController">Profissões</a> </li>-->
						</ul>
					</li>
					<li> <a href="<?=base_url()?>empresaController/editarEmpresaAction/1"><i class="fa fa-home fa-fw"></i> Dados do gabinete </a> </li> 
				<? if($this->session->userdata("tipo") == 'Administrador'){ ?>	
					<li> <a href="<?=base_url()?>usuarioController"><i class="fa fa-lock fa-fw"></i> Usuários </a> </li>
				<?}?>	
                </ul>
            </div>
        </nav>
	   
	   