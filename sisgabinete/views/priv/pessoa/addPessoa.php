<?
$this->load->view('priv/_inc/superior');
?>

<script type="text/javascript">
	$().ready(function() {
		$("#nome").autocomplete("<?=base_url()?>pessoaController/buscarNomes", {
			//width: 260,
			matchContains: true,
			//mustMatch: true,
			//minChars: 0,
			//multiple: true,
			//highlight: false,
			//multipleSeparator: ",",
			selectFirst: true
		});
		$("#profissao").autocomplete("<?=base_url()?>pessoaController/buscarProfissoes", {
			//width: 260,
			matchContains: true,
			//mustMatch: true,
			//minChars: 0,
			//multiple: true,
			//highlight: false,
			//multipleSeparator: ",",
			selectFirst: true
		});
		$("#trabalho").autocomplete("<?=base_url()?>pessoaController/buscarTrabalho", {
			//width: 260,
			matchContains: true,
			//mustMatch: true,
			//minChars: 0,
			//multiple: true,
			//highlight: false,
			//multipleSeparator: ",",
			selectFirst: true
		});
		$("#email").autocomplete("<?=base_url()?>pessoaController/buscarEmail", {
			//width: 260,
			matchContains: true,
			//mustMatch: true,
			//minChars: 0,
			//multiple: true,
			//highlight: false,
			//multipleSeparator: ",",
			selectFirst: true
		});
		$("#cidade").autocomplete("<?=base_url()?>pessoaController/buscarCidade", {
			//width: 260,
			matchContains: true,
			//mustMatch: true,
			//minChars: 0,
			//multiple: true,
			//highlight: false,
			//multipleSeparator: ",",
			selectFirst: true
		});
	});
</script>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Pessoas</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><a href="<?= base_url() ?>principal/arearestrita">Principal</a> &raquo; <a href="<?= BASE_URL(); ?>pessoaController/">Pessoas</a> &raquo; Cadastrar</div>
			</div>
			<form method="post" action="addPessoa" autocomplete="off">
				<div class="row">
					<div class="col-lg-3">
						<div class="form-group">
							<label>Data de cadastro</label><br>
							<input type="text" name="dataCadastro" id="dataCadastro" value="<?=date("d/m/Y")?>" class="data form-control" />
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>Nome</label><br>
							<input type="text" name="nome" id="nome" class="form-control" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>E-mail</label><br>
							<input type="text" name="email" id="email" class="form-control" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Data de nascimento</label><br>
							<input type="text" name="nascimento" id="nascimento" class="data form-control" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Telefone fixo</label><br>
							<input type="text" name="fixo" id="fixo" class="telefone form-control" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Celular</label><br>
							<input type="text" name="celular" id="celular" class="telefone form-control" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Telefone para contato</label><br>
							<input type="text" name="outro" id="outro" class="telefone form-control" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>CEP</label><br>
							<input type="text" name="cep" id="cep" class="cep form-control" />
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>Rua</label><br>
							<input type="text" name="rua" id="rua" class="form-control" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Número</label><br>
							<input type="text" name="numero" id="numero" class="form-control" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Bairro</label><br>
							<input type="text" name="bairro" id="bairro" class="form-control" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Complemento</label><br>
							<input type="text" name="complemento" id="complemento" class="form-control" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Cidade</label><br>
							<input type="text" name="cidade" id="cidade" class="form-control">
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Estado</label><br>
							<select name="estado" id="estado" class="form-control">
								<option value=""></option>
								<option value="AC">Acre</option>
								<option value="AL">Alagoas</option>
								<option value="AM">Amazonas</option>
								<option value="AP">Amapá</option>
								<option value="BA">Bahia</option>
								<option value="CE">Ceará</option>
								<option value="DF">Distrito Federal</option>
								<option value="ES">Espirito Santo</option>
								<option value="GO">Goiás</option>
								<option value="MA">Maranhão</option>
								<option value="MG">Minas Gerais</option>
								<option value="MS">Mato Grosso do Sul</option>
								<option value="MT">Mato Grosso</option>
								<option value="PA">Pará</option>
								<option value="PB">Paraíba</option>
								<option value="PE">Pernambuco</option>
								<option value="PI">Piauí</option>
								<option value="PR">Paraná</option>
								<option value="RJ">Rio de Janeiro</option>
								<option value="RN">Rio Grande do Norte</option>
								<option value="RO">Rondônia</option>
								<option value="RR">Roraima</option>
								<option value="RS">Rio Grande do Sul</option>
								<option value="SC">Santa Catarina</option>
								<option value="SE">Sergipe</option>
								<option value="SP">São Paulo</option>
								<option value="TO">Tocantins</option>
							</select>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Profissão</label> <br>
							<input type="text" name="profissao" id="profissao" value="<?=$row->profissao?>" class="form-control" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Referência</label> <br>
							<input type="text" name="referencia" id="referencia" value="<?=$row->referencia?>" class="form-control" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Local de trabalho</label> <br>
							<input type="text" name="trabalho" id="trabalho" value="<?=$row->trabalho?>" class="form-control" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Região</label> <br>
							<select name="idregiao" id="idregiao" class="form-control">
								<option value=""></option>
								<? foreach ($regiao as $r) { ?>
								<option value="<?=$r->id?>"><?=$r->titulo?></option>
								<? } ?>
							</select>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<input type="button" value="Voltar" class="btn btn-default" onclick="location.href='<?= base_url() ?>pessoaController'"  />
							<input type="submit" class="btn btn-success" name="btSalvar" value="Salvar" />
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?
$this->load->view('priv/_inc/inferior');
?>
