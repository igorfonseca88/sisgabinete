<?
$this->load->view('priv/_inc/superior');
?>

<script type="text/javascript">
	function confirmaExcluir(id) {
		if (confirm("Deseja excluir este atendimento?")) {
			window.location.href = "<?= base_url() ?>pessoaController/deleteAtendimento/" + id;
		} else {
			return false;
		}
	}
	$().ready(function() {
		$("#nome").autocomplete("<?=base_url()?>pessoaController/buscarNomes", {
			//width: 260,
			matchContains: true,
			//mustMatch: true,
			//minChars: 0,
			//multiple: true,
			//highlight: false,
			//multipleSeparator: ",",
			selectFirst: true
		});
		$("#profissao").autocomplete("<?=base_url()?>pessoaController/buscarProfissoes", {
			//width: 260,
			matchContains: true,
			//mustMatch: true,
			//minChars: 0,
			//multiple: true,
			//highlight: false,
			//multipleSeparator: ",",
			selectFirst: true
		});
		$("#trabalho").autocomplete("<?=base_url()?>pessoaController/buscarTrabalho", {
			//width: 260,
			matchContains: true,
			//mustMatch: true,
			//minChars: 0,
			//multiple: true,
			//highlight: false,
			//multipleSeparator: ",",
			selectFirst: true
		});
		$("#email").autocomplete("<?=base_url()?>pessoaController/buscarEmail", {
			//width: 260,
			matchContains: true,
			//mustMatch: true,
			//minChars: 0,
			//multiple: true,
			//highlight: false,
			//multipleSeparator: ",",
			selectFirst: true
		});
		$("#cidade").autocomplete("<?=base_url()?>pessoaController/buscarCidade", {
			//width: 260,
			matchContains: true,
			//mustMatch: true,
			//minChars: 0,
			//multiple: true,
			//highlight: false,
			//multipleSeparator: ",",
			selectFirst: true
		});
	});
</script>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Pessoas
				<a class="btn btn-success" onclick="$('html,body').animate({scrollTop: $('#divAtendimento').offset().top}, 1000);" style="float:right">Novo atendimento</a>
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><a href="<?= base_url() ?>principal/arearestrita">Principal</a> &raquo; <a href="<?= BASE_URL(); ?>pessoaController/">Pessoas</a> &raquo; Editar</div>
			</div>
		</div>
	</div>
			
			
	<? foreach ($pessoa as $row) { ?>
	<div class="row">
		<div class="col-lg-12">	
			<?= $sucesso != "" ? '<div class="alert alert-success"> ' . $sucesso . ' </div>' : "" ?>
			<?= $erro != "" ? '<div class="alert alert-danger"> ' . $erro . ' </div>' : "" ?>			
			<form method="post" action="<?= base_url() ?>pessoaController/editPessoa">
				<input type="hidden" name="id" id="id" value="<?= $row->id ?>"/>
				<div class="row">
					<div class="col-lg-3">
						<div class="form-group">
							<label>Data do cadastro</label><br>
							<input type="text" name="dataCadastro" id="dataCadastro" class="form-control data" value="<?=implode("/",array_reverse(explode("-",$row->dataCadastro)))?>" />
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>Nome</label><br>
							<input type="text" name="nome" id="nome" class="form-control" value="<?=$row->nome?>" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>E-mail</label><br>
							<input type="text" name="email" id="email" class="form-control" value="<?=$row->email?>" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Data de nascimento</label><br>
							<input type="text" name="nascimento" id="nascimento" class="data form-control" value="<?=implode("/",array_reverse(explode("-",$row->nascimento)))?>" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Telefone fixo</label><br>
							<input type="text" name="fixo" id="fixo" class="telefone form-control"  value="<?=$row->fixo?>" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Celular</label><br>
							<input type="text" name="celular" id="celular" class="telefone form-control"  value="<?=$row->celular?>" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Telefone para contato</label><br>
							<input type="text" name="outro" id="outro" class="telefone form-control"  value="<?=$row->outro?>" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>CEP</label><br>
							<input type="text" name="cep" id="cep" class="cep form-control" value="<?=$row->cep?>" />
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>Rua</label><br>
							<input type="text" name="rua" id="rua" class="form-control"  value="<?=$row->rua?>" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Número</label><br>
							<input type="text" name="numero" id="numero" class="form-control"  value="<?=$row->numero?>" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Bairro</label><br>
							<input type="text" name="bairro" id="bairro" class="form-control"  value="<?=$row->bairro?>" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Complemento</label><br>
							<input type="text" name="complemento" id="complemento" class="form-control"  value="<?=$row->complemento?>" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Cidade</label><br>
							<input type="text" name="cidade" id="cidade" class="form-control" value="<?=$row->cidade?>" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Estado</label><br>
							<select name="estado" id="estado" class="form-control">
								<option value=""></option>
								<option <?= $row->estado == "AC" ? "selected" : "" ?> value="AC">Acre</option>
								<option <?= $row->estado == "AL" ? "selected" : "" ?> value="AL">Alagoas</option>
								<option <?= $row->estado == "AM" ? "selected" : "" ?> value="AM">Amazonas</option>
								<option <?= $row->estado == "AP" ? "selected" : "" ?> value="AP">Amapá</option>
								<option <?= $row->estado == "BA" ? "selected" : "" ?> value="BA">Bahia</option>
								<option <?= $row->estado == "CE" ? "selected" : "" ?> value="CE">Ceará</option>
								<option <?= $row->estado == "DF" ? "selected" : "" ?> value="DF">Distrito Federal</option>
								<option <?= $row->estado == "ES" ? "selected" : "" ?> value="ES">Espirito Santo</option>
								<option <?= $row->estado == "GO" ? "selected" : "" ?> value="GO">Goiás</option>
								<option <?= $row->estado == "MA" ? "selected" : "" ?> value="MA">Maranhão</option>
								<option <?= $row->estado == "MG" ? "selected" : "" ?> value="MG">Minas Gerais</option>
								<option <?= $row->estado == "MS" ? "selected" : "" ?> value="MS">Mato Grosso do Sul</option>
								<option <?= $row->estado == "MT" ? "selected" : "" ?> value="MT">Mato Grosso</option>
								<option <?= $row->estado == "PA" ? "selected" : "" ?> value="PA">Pará</option>
								<option <?= $row->estado == "PB" ? "selected" : "" ?> value="PB">Paraíba</option>
								<option <?= $row->estado == "PE" ? "selected" : "" ?> value="PE">Pernambuco</option>
								<option <?= $row->estado == "PI" ? "selected" : "" ?> value="PI">Piauí</option>
								<option <?= $row->estado == "PR" ? "selected" : "" ?> value="PR">Paraná</option>
								<option <?= $row->estado == "RJ" ? "selected" : "" ?> value="RJ">Rio de Janeiro</option>
								<option <?= $row->estado == "RN" ? "selected" : "" ?> value="RN">Rio Grande do Norte</option>
								<option <?= $row->estado == "RO" ? "selected" : "" ?> value="RO">Rondônia</option>
								<option <?= $row->estado == "RR" ? "selected" : "" ?> value="RR">Roraima</option>
								<option <?= $row->estado == "RS" ? "selected" : "" ?> value="RS">Rio Grande do Sul</option>
								<option <?= $row->estado == "SC" ? "selected" : "" ?> value="SC">Santa Catarina</option>
								<option <?= $row->estado == "SE" ? "selected" : "" ?> value="SE">Sergipe</option>
								<option <?= $row->estado == "SP" ? "selected" : "" ?> value="SP">São Paulo</option>
								<option <?= $row->estado == "TO" ? "selected" : "" ?> value="TO">Tocantins</option>
							</select>
						</div>
					</div>					
					<div class="col-lg-3">
						<div class="form-group">
							<label>Profissão</label> <br>
							<input type="text" name="profissao" id="profissao" value="<?=$row->profissao?>" class="form-control"  />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Referência</label> <br>
							<input type="text" name="referencia" id="referencia" value="<?=$row->referencia?>" class="form-control" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Local de trabalho</label> <br>
							<input type="text" name="trabalho" id="trabalho" value="<?=$row->trabalho?>" class="form-control" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Região</label> <br>
							<select name="idregiao" id="idregiao" class="form-control">
								<option value=""></option>
								<? foreach ($regiao as $r) { ?>
								<option <?= $row->idregiao == $r->id ? "selected" : "" ?> value="<?=$r->id?>"><?=$r->titulo?></option>
								<? } ?>
							</select>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<input type="button" value="Voltar" class="btn btn-default" onclick="location.href='<?= base_url() ?>pessoaController'"  />
							<input type="submit" class="btn btn-success" name="btSalvar" value="Salvar" />
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	
	<br><br>
		
	<div class="row">
		<div id="divAtendimento" class="col-lg-12">
			<h1 class="page-header">Lista de Atendimentos</h1>
		</div>
	</div>	
	<div class="row">
		<div class="col-lg-12">
			<table class="table table-striped table-bordered table-hover" id="dataTables-example">
				<thead>					
					<th width="150">Data do atendimento</th>
					<th>Assunto</th>
					<th>Local Atendimento</th>
					<th>Ocorrência</th>
					<th width="120">Situação</th>
					<th width="120" align="center">Ações</th>
				</thead>
				<? foreach ($atendimentos as $at) { ?>
				 <tr>
					<td> <?= implode("/",array_reverse(explode("-",$at->data))) ?></td>
					<td> <?= $at->assunto ?></td>
					<td> <?= $at->local ?></td>
					<td> <?= $at->ocorrencia ?></td>
					<td> <?= $at->situacao ?></td>
					<td align="center">
						<a href="<?= base_url() ?>pessoaController/editarAtendimentoAction/<?= $at->id ?>/<?=$row->id?>#divAtendimento">Editar</a> |
						<a onclick="confirmaExcluir(<?= $at->id ?>)">Excluir</a> 
					</td>
				 </tr>
			  <? } ?>
		   </table>
		</div>
	</div>
	
	<br><br>
	
	<div class="row">
		<? if($editAtendimento == ""){?>
		<div id="divAtendimento" class="col-lg-12">
			<h1 class="page-header">Novo atendimento</h1>
		</div>
		<div class="col-lg-12">
			<div class="row">
				<form method="post" action="<?= BASE_URL(); ?>pessoaController/novoAtendimento">
					<input type="hidden" name="idpessoa" id="idpessoa" value="<?= $row->id ?>"/>
					<div class="col-lg-2">					
						<div class="form-group">
							<label>Data do atendimento</label><br>
							<input type="text" name="data" id="data" class="form-control data" value="<?=date("d/m/Y")?>" />
						</div>
					 </div>	
					<div class="col-lg-4">
						<div class="form-group">
							<label>Assunto</label> <br>
							<select name="idAssunto" id="idAssunto" class="form-control">
								<option value="">Selecione</option>
								<? foreach ($assunto as $a) { ?>
								<option  value="<?=$a->id?>"><?=$a->assunto?></option>
								<? } ?>
							</select>
						</div>
					</div>	
					<div class="col-lg-3">
						<div class="form-group">
							<label>Local do atendimento</label> <br>
							<input type="text" name="local" id="local" class="form-control" value="" />
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Atendido por:</label> <br>
							<select name="idUsuario" id="idUsuario" class="form-control">
								<option value="">Selecione</option>
								<? foreach ($usuarios as $u) { ?>
								<option <?= $this->session->userdata("idUsuario") == $u->idUsuario ? "selected" : "" ?> value="<?=$u->idUsuario?>"><?=$u->nome?></option>
								<? } ?>
							</select>
						</div>
					</div>					
					<div class="col-lg-12">
						<div class="form-group">
							<label>Ocorrência</label><br>
							<textarea name="ocorrencia" id="ocorrencia" class="form-control"></textarea>
						</div>
					</div>						
					<div class="col-lg-12">
						<div class="form-group">
							<label>Situação</label> <br>
							<select name="situacao" id="situacao" class="form-control">
								<option value="">Selecione</option>								
								<option  value="Em aberto">Em aberto</option>
								<option  value="Finalizado">Finalizado</option>								
							</select>
						</div>
					</div>						
					<div class="col-lg-12">
						<div class="form-group">
							<input type="reset" value="Cancelar" class="btn btn-default" />
							<input type="submit" class="btn btn-success" name="btSalvar" value="Salvar" />
						</div>
					</div>
				</div>
			</form>
		</div>
		<?}else if($editAtendimento == "S"){?>
		<div id="divAtendimento" class="col-lg-12">
			<h1 class="page-header">Editar atendimento</h1>
		</div>
		<form method="post" action="<?= BASE_URL(); ?>pessoaController/editarAtendimento">
			<input type="hidden" name="idpessoa" id="idpessoa" value="<?= $row->id ?>"/>
			<? foreach ($atendimentoEdit as $atend) { ?>	
			<input type="hidden" name="idAtendimento" id="idAtendimento" value="<?= $atend->id ?>"/>
			<div class="col-lg-2">					
				<div class="form-group">
					<label>Data do atendimento</label><br>
					<input type="text" name="data" id="data" class="form-control data" value="<?=implode("/",array_reverse(explode("-",$atend->data)))?>" />
				</div>
			 </div>	
			<div class="col-lg-4">
				<div class="form-group">
					<label>Assunto</label> <br>
					<select name="idAssunto" id="idAssunto" class="form-control">
						<option value="">Selecione</option>
						<? foreach ($assunto as $as) { ?>
						<option <?= $atend->idAssunto == $as->id ? "selected" : "" ?>  value="<?=$as->id?>"><?=$as->assunto?></option>
						<? } ?>
					</select>
				</div>
			</div>					
			<div class="col-lg-3">
				<div class="form-group">
					<label>Local do atendimento</label> <br>
					<input type="text" name="local" id="local" class="form-control" value="<?=$atend->local?>" />
				</div>
			</div>
			<div class="col-lg-3">
				<div class="form-group">
					<label>Atendido por:</label> <br>
					<select name="idUsuario" id="idUsuario" class="form-control">
						<option value="">Selecione</option>								
						<? foreach ($usuarios as $u) { ?>
						<option <?= $atend->idUsuario == $u->idUsuario ? "selected" : "" ?>  value="<?=$u->idUsuario?>"><?=$u->nome?></option>
						<? } ?>
					</select>
				</div>
			</div>				
			<div class="col-lg-12">
				<div class="form-group">
					<label>Ocorrência</label><br>
					<textarea name="ocorrencia" id="ocorrencia" class="form-control"><? echo $atend->ocorrencia ?></textarea>
				</div>
			</div>					
			<div class="col-lg-12">
				<div class="form-group">
					<label>Situação</label> <br>
					<select name="situacao" id="situacao" class="form-control">
						<option value="">Selecione</option>						
						<option <?= $atend->situacao == 'Em aberto' ? "selected" : "" ?>  value="Em aberto">Em aberto</option>
						<option <?= $atend->situacao == 'Finalizado' ? "selected" : "" ?> value="Finalizado">Finalizado</option>						
					</select>
				</div>
			</div>	
			<div class="col-lg-12">
				<div class="form-group">
					<input type="button" value="Cancelar" class="btn btn-default" onclick="location.href='<?= base_url() ?>pessoaController/editarPessoaAction/<?= $row->id?>#divAtendimento'"  />
					<input type="submit" class="btn btn-success" name="btSalvar" value="Salvar" />
				</div>
			</div>
			<?}?>	
		</form>
		<?}?>
	
	</div>
	<? } ?>
</div>



<?
$this->load->view('priv/_inc/inferior');
?>
