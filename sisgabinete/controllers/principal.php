<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?php
session_start();

class Principal extends CI_Controller {

	
	function __construct() {
		parent::__construct();
	}
	
	public function arearestrita() {		
	
		if($this->session->userdata("idEmpresa") == null || $this->session->userdata("idEmpresa") == ""){
			redirect("/");
		}
	
		// busca aniversariantes da semana
		$this->load->model('Pessoa_model', 'pessoa');
		$data["aniversariantes"] = $this->pessoa->buscarAniversariantes($this->session->userdata("idEmpresa"));
		$data["ultimasAtividades"] = $this->pessoa->buscarUltimasAtividades($this->session->userdata("idEmpresa"));

		$this->load->vars($data);
		$this->load->view('priv/default');
	}
		
}
?>