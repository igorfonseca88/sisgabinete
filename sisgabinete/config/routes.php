<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = 'login/login';
$route['/'] = "principal";
$route['login'] = 'login/login';
$route['404_override'] = 'principal/erro404';

?>
