<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?php

class AssuntoController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }
	
	function index()
	{
		$this->load->model('Assunto_model', 'assunto');
		$data["assunto"] = $this->assunto->getAll();
		
		$this->load->vars($data);
		$this->load->view("priv/assunto/listAssunto");
	}
	
	function novoAssuntoAction()
	{
		$this->load->view("priv/assunto/addAssunto");
	}
	
	function addAssunto()
	{
		$this->load->model("Assunto_model", "assunto");		
		$insert = array(
			"assunto" => $this->input->post("assunto")
		);
		$id = $this->assunto->add_record($insert);
		
		if ($id > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["error"] = "Erro ao salvar.";
		}
		$data["assunto"] = $this->assunto->getAll();		
		$this->load->vars($data);
		$this->load->view("priv/assunto/listAssunto");
	}
	
	function deleteAssunto($id)
	{
		$this->load->model("Assunto_model", "assunto");		

		if ($this->assunto->delete($id) > 0) {
			$data["sucesso"] = "Excluído com sucesso.";
		} else {
			$data["error"] = "Erro ao excluir.";
		}
				
		$data["assunto"] = $this->assunto->getAll();		
		$this->load->vars($data);
		$this->load->view("priv/assunto/listAssunto");
	}

	function editAssunto() {
		$this->load->model("Assunto_model", "assunto");
		$id = $this->input->post("id");
		
		$update = array(
			"assunto" => $this->input->post("assunto")
		);
		
		if ($this->assunto->update($id,$update) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["erro"] = "Erro ao salvar.";
		}
		
		$data["assunto"] = $this->assunto->getAll();		
		$this->load->vars($data);
		$this->load->view("priv/assunto/listAssunto");
    }
    
    function editarAssuntoAction($id, $mensagem = array()) {   
        $this->load->model('Assunto_model', 'assunto');
        $data["assunto"] = $this->assunto->buscarPorId($id);
		
        $data["sucesso"] = $mensagem["sucesso"];
        $data["error"] = $mensagem["error"];
			   
        $this->load->vars($data);
        $this->load->view("priv/assunto/editAssunto");
    } 
}

?>