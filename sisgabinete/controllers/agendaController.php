<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?php

class AgendaController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }
	
	function index()
	{
		if($this->session->userdata("idEmpresa") == null || $this->session->userdata("idEmpresa") == ""){
			redirect("/");
		}
		
		$this->load->model('Agenda_model', 'agenda');
		$data["agenda"] = $this->agenda->buscarAgendaPorEmpresa($this->session->userdata("idEmpresa"));
		
		$this->load->vars($data);
		$this->load->view("priv/agenda/listAgenda");
	}
	
	function novaAgendaAction()
	{
		$this->load->view("priv/agenda/addAgenda");
	}
	
	function addAgenda()
	{
		$this->load->model("Agenda_model", "agenda");		
		$insert = array(
			"titulo" => $this->input->post("titulo"),
			"data" => implode("-",array_reverse(explode("/",$this->input->post("data")))),
			"hora" => $this->input->post("hora"),
			"local" => $this->input->post("local"),
			"contato" => $this->input->post("contato"),
			"telefone" => $this->input->post("telefone"),
			"observacoes" => $this->input->post("observacoes"),
			"idEmpresa" => $this->session->userdata("idEmpresa")
		);
		
		if ($this->agenda->add_record($insert) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["error"] = "Erro ao salvar.";
		}
		$this->editarAgendaAction($id, $data);
	}
	
	function deleteAgenda($id)
	{
		$this->load->model("Agenda_model", "agenda");		

		if ($this->agenda->delete($id) > 0) {
			$data["sucesso"] = "Excluído com sucesso.";
		} else {
			$data["error"] = "Erro ao excluir.";
		}
		
		$data["agenda"] = $this->agenda->getAll();
		$this->load->vars($data);
		$this->load->view("priv/agenda/listAgenda");
	}

	function editAgenda() {
		$this->load->model("Agenda_model", "agenda");
		$id = $this->input->post("id");
		
		$update = array(
			"titulo" => $this->input->post("titulo"),
			"data" => implode("-",array_reverse(explode("/",$this->input->post("data")))),
			"hora" => $this->input->post("hora"),
			"local" => $this->input->post("local"),
			"contato" => $this->input->post("contato"),
			"telefone" => $this->input->post("telefone"),
			"observacoes" => $this->input->post("observacoes")
		);
		
		if ($this->agenda->update($id,$update) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["erro"] = "Erro ao salvar.";
		}
		
		$this->editarAgendaAction($id, $data);
    }
    
    function editarAgendaAction($id, $mensagem = array()) {   
        $this->load->model('Agenda_model', 'agenda');
        $data["agenda"] = $this->agenda->buscarPorId($id);
		
        $data["sucesso"] = $mensagem["sucesso"];
        $data["error"] = $mensagem["error"];
			   
        $this->load->vars($data);
        $this->load->view("priv/agenda/editAgenda");
    } 
}

?>