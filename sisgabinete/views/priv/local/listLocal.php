<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?
$this->load->view('priv/_inc/superior');
?>

<script>
	function confirmaExcluir(id) {
		var r=confirm("Deseja excluir este local de atendimento?")
		if (r==true) { location.href = "<?= base_url() ?>localController/deleteLocal/" + id; }
	}
</script>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Locais de atendimento
				<input type="button" class="btn btn-success" name="btNovo" onclick="location.href='<?= base_url() ?>localController/novoLocalAction'"  style="float:right" value="Cadastrar" />
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><a href="<?= base_url() ?>principal/arearestrita">Principal</a> &raquo; Locais de atendimento </div>
			</div>
				
			<?= $sucesso != "" ? '<div class="alert alert-success"> ' . $sucesso . ' </div>' : "" ?>
			<?= $erro != "" ? '<div class="alert alert-danger"> ' . $erro . ' </div>' : "" ?>
			
			<table class="table table-striped table-bordered table-hover" id="dataTables-example">
				<thead>
					<th>Local</th>
					<th width="120" align="center">Ações</th>
				</thead>
				<? foreach ($local as $row) { ?>
				 <tr>
					<td> <?= $row->titulo ?></td>
					<td align="center">
						<a href="<?= base_url() ?>localController/editarLocalAction/<?= $row->id ?>">Editar</a> |
						<a onclick="confirmaExcluir('<?= $row->id ?>')">Excluir</a>
					</td>
				 </tr>
			  <? } ?>
		   </table>
	    </div>
	</div>
</div>

<?
$this->load->view('priv/_inc/inferior');
?>