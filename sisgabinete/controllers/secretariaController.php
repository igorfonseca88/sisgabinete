<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?php

class SecretariaController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }
	
	function index()
	{
		$this->load->model('Secretaria_model', 'secretaria');
		$data["secretaria"] = $this->secretaria->getAll();
		
		$this->load->vars($data);
		$this->load->view("priv/secretaria/listSecretaria");
	}
	
	function novaSecretariaAction()
	{
		$this->load->view("priv/secretaria/addSecretaria");
	}
	
	function addSecretaria()
	{
		$this->load->model("Secretaria_model", "secretaria");		
		$insert = array(
			"titulo" => $this->input->post("titulo")
		);
		
		if ($this->secretaria->add_record($insert) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["error"] = "Erro ao salvar.";
		}
		
		$data["secretaria"] = $this->secretaria->getAll();
		$this->load->vars($data);
		$this->load->view("priv/secretaria/listSecretaria");
	}
	
	function deleteSecretaria($id)
	{
		$this->load->model("Secretaria_model", "secretaria");		

		if ($this->secretaria->delete($id) > 0) {
			$data["sucesso"] = "Excluído com sucesso.";
		} else {
			$data["error"] = "Erro ao excluir.";
		}
		
		$data["secretaria"] = $this->secretaria->getAll();
		$this->load->vars($data);
		$this->load->view("priv/secretaria/listSecretaria");
	}

	function editSecretaria() {
		$this->load->model("Secretaria_model", "secretaria");
		$id = $this->input->post("id");
		
		$update = array(
			"titulo" => $this->input->post("titulo")
		);
		
		if ($this->secretaria->update($id,$update) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["erro"] = "Erro ao salvar.";
		}		
		
		$data["secretaria"] = $this->secretaria->getAll();
		$this->load->vars($data);
		$this->load->view("priv/secretaria/listSecretaria");
    }
    
    function editarSecretariaAction($id, $mensagem = array()) {   
        $this->load->model('Secretaria_model', 'secretaria');
        $data["secretaria"] = $this->secretaria->buscarPorId($id);
		
        $data["sucesso"] = $mensagem["sucesso"];
        $data["error"] = $mensagem["error"];
			   
        $this->load->vars($data);
        $this->load->view("priv/secretaria/editSecretaria");
    } 
}

?>