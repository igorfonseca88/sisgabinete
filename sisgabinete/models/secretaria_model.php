<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?php
class Secretaria_model extends CI_Model {
    
    function getAll() {
        $query = $this->db->query("select * from tb_secretaria order by titulo asc");
        return $query->result();
    }
	
    function add_record($options = array()) {
        $this->db->insert('tb_secretaria', $options);
        return $this->db->insert_id();
    }
    
    function update($id, $options = array()) {
        $this->db->where('id', $id);
        $this->db->update('tb_secretaria',$options);
        return $this->db->affected_rows();
    }
    
    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('tb_secretaria',$options);
        return $this->db->affected_rows();
    }
    
    function buscarPorId($id) {
        $this->db->where('id', $id);
        $query = $this->db->get("tb_secretaria");
        return $query->result();
    }
}
?>