<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?php
class Pessoa_model extends CI_Model {
    
    function getAll() {
        $query = $this->db->query("select * from tb_pessoa order by nome asc");
        return $query->result();
    }
    
	// select p.id, p.nome, p.email, p.fixo, p.celular, r.titulo as regiao, (SELECT CONCAT(CONCAT( a.local,  ' - ', ass.assunto ), ' - ', a.ocorrencia)  FROM tb_atendimento a join tb_assunto ass on ass.id = a.idAssunto WHERE a.idpessoa = p.id ORDER BY a.data DESC  LIMIT 0 , 1) as atendimento from tb_pessoa p left join tb_regiao r on p.idregiao = r.id order by nome asc
	function buscarPessoas($idEmpresa) {
        $sql = "select p.id, p.nome, p.email, p.fixo, p.celular, (SELECT CONCAT(CONCAT( a.local,  ' - ', ass.assunto ), ' - ', a.ocorrencia) 
										FROM tb_atendimento a
										join tb_assunto ass on ass.id = a.idAssunto
										WHERE a.idpessoa = p.id
										ORDER BY a.data DESC 
										LIMIT 0 , 1) as atendimento  from tb_pessoa p  WHERE idEmpresa = ? order by nome asc";
        $query = $this->db->query($sql, array($idEmpresa));
		return $query->result();
    }
    
    function buscarPessoaCompleto() {
        $query = $this->db->query("select * from tb_pessoa order by nome asc");
        return $query->result();
    }
    
    function buscarPessoasSemDataNasc() {
        $query = $this->db->query("select p.id, p.nome, p.email, p.fixo as telefone from tb_pessoa p where nascimento = null or nascimento = '' or nascimento = '0000-00-00' order by nome asc");
        return $query->result();
    }
	
	function buscarPessoasPorNome($nome, $idEmpresa) {
		$sql = "select p.id, p.nome, p.email, p.fixo, p.celular, r.titulo as regiao, 
									(SELECT CONCAT(CONCAT( a.local,  ' - ', ass.assunto ), ' - ', a.ocorrencia) 
										FROM tb_atendimento a
										join tb_assunto ass on ass.id = a.idAssunto
										WHERE a.idpessoa = p.id
										ORDER BY a.data DESC 
										LIMIT 0 , 1) as atendimento 
						           from tb_pessoa p left join tb_regiao r on p.idregiao = r.id WHERE p.nome like '%$nome%' p.idEmpresa = $idEmpresa order by nome asc";
		$query = $this->db->query($sql);
        return $query->result();
    }
	
	function buscarProfissoes($q){
		$query = $this->db->query("select distinct profissao from tb_pessoa where profissao LIKE '$q%' group by profissao order by profissao asc");
        return $query->result();
	}
	
	function buscarCidade($q){
		$query = $this->db->query("select distinct nome from cidades where nome LIKE '$q%' and estado_cod = 12 group by nome order by nome asc");
        return $query->result();
	}
	
	function buscarNomes($q){
		$query = $this->db->query("select distinct nome from tb_pessoa where nome LIKE '$q%' group by nome order by nome asc");
        return $query->result();
	}
	
	function buscarTrabalho($q){
		$query = $this->db->query("select distinct trabalho from tb_pessoa where trabalho LIKE '$q%' group by trabalho order by trabalho asc");
        return $query->result();
	}
	
	function buscarEmail($q){
		$query = $this->db->query("select distinct email from tb_pessoa where email LIKE '$q%' group by email order by email asc");
        return $query->result();
	}
	
	function buscarEmails(){
		$query = $this->db->query("select distinct email from tb_pessoa order by email asc");
        return $query->result();
	}
	
	function buscarProfessoresEscolas(){
		$query = $this->db->query("SELECT nome, email, fixo, celular, outro, cep, endereco, numero, bairro, complemento, cidade, estado, trabalho, profissao FROM `tb_pessoa` WHERE `trabalho` like '%profe%' or `trabalho` like '%educ%'  or `trabalho` like '%escola%' or `profissao` like '%profe%' or `profissao` like '%educ%'  or `profissao` like '%escola%' or `nome` like '%escola%' or `nome` like '%prof%'");
        return $query->result();
	}
	
	function buscarAniversariantes($idEmpresa) {
        $query = $this->db->query(" Select *, (SELECT CONCAT(CONCAT( a.local,  ' - ', ass.assunto ), ' - ', a.ocorrencia) 
										FROM tb_atendimento a
										join tb_assunto ass on ass.id = a.idAssunto
										WHERE a.idpessoa = p.id
										ORDER BY a.data DESC 
										LIMIT 0 , 1) as atendimento   FROM tb_pessoa p
		WHERE CONCAT( YEAR(NOW()),'-',MONTH(nascimento),'-',DAY(nascimento) ) between DATE_ADD(CURDATE(), INTERVAL -1 DAY) 
		AND DATE_ADD(CURDATE(), INTERVAL 7 DAY) and p.idEmpresa = $idEmpresa
		order by DAY(nascimento)  ");
		
        return $query->result();
    }	
	
	function buscarPessoasPorAtendimento($id) {
        $query = $this->db->query("select p.id, p.nome, p.email, p.fixo, p.celular,p.outro, p.cep, p.endereco, p.numero, p.bairro, p.complemento, p.cidade, p.estado from tb_pessoa p where p.id in (SELECT DISTINCT a.idpessoa from tb_atendimento a where a.idAssunto = $id) order by p.nome asc ");
        return $query->result();
    }
    
    function add_record($options = array()) {
        $this->db->insert('tb_pessoa', $options);
        return $this->db->insert_id();
    }
    
    function update($id, $options = array()) {
        $this->db->where('id', $id);
        $this->db->update('tb_pessoa',$options);
        return $this->db->affected_rows();
    }
    
    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('tb_pessoa',$options);
        return $this->db->affected_rows();
    }
    
    function buscarPorId($id, $idEmpresa) {
        

        $sql = "select * from tb_pessoa WHERE id = ? and idEmpresa";
        $query = $this->db->query($sql, array($id, $idEmpresa));
		return $query->result();
    }

	function buscarUltimasAtividades($idEmpresa) {
        $query = $this->db->query(" Select *, (SELECT CONCAT(CONCAT( a.local,  ' - ', ass.assunto ), ' - ', a.ocorrencia) 
										FROM tb_atendimento a
										join tb_assunto ass on ass.id = a.idAssunto
										WHERE a.idpessoa = p.id
										ORDER BY a.data DESC 
										LIMIT 0 , 1) as atendimento   FROM tb_pessoa p
		WHERE  p.idEmpresa = $idEmpresa
		  ");
		
        return $query->result();
    }	
}
?>