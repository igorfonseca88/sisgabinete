<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?php

class EmpresaController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->load->model('Empresa_model', 'empresa');
        $data["empresa"] = $this->empresa->getAll();
		$this->load->model("Estado_model", "estado");
        $data["estados"] = $this->estado->getAll();
        $this->load->vars($data);
        $this->load->view("priv/empresa/editEmpresa");
    }    
    
    function editarEmpresaAction($id) {
        $this->load->model("Empresa_model", "empresa");
		$this->load->model("Estado_model", "estado");
        $data["estados"] = $this->estado->getAll();
		$data["empresa"] = $this->empresa->buscarPorId($id);
		
		$this->load->vars($data);
		$this->load->view("priv/empresa/editEmpresa");
    }

    function editEmpresa() {
        $this->load->model("Empresa_model", "empresa");
        $id = $this->input->post("id");
        $update = array(            
			"razaoSocial" => $this->input->post("razaoSocial"),
			"telefone" => $this->input->post("telefone"),
			"email" => $this->input->post("email"),
			"smtp" => $this->input->post("smtp"),
			"porta" => $this->input->post("porta"),
			"usuario" => $this->input->post("usuario"),
			"senha" => $this->input->post("senha"),
			"esfera" => $this->input->post("esfera"),
			"estado" => $this->input->post("estado"),
			"cidade" => $this->input->post("cidade")
			
        );

        if ($this->empresa->update($id, $update) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
			$data["empresa"] = $this->empresa->getAll();
			$this->load->model("Estado_model", "estado");
			$data["estados"] = $this->estado->getAll();
			
			$this->load->vars($data);
			$this->load->view("priv/empresa/editEmpresa");
        }
    }
}

?>