<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?php
session_start();

class Login extends CI_Controller {
	
	function __construct() {
		parent::__construct();
	}
	
	function index() {
		if ($this->Usuario_model->logged() == TRUE && $this->Usuario_model->usuarioExiste() == TRUE) { redirect('principal/arearestrita'); }
		$this->load->view('priv/login/login_view');
	}
	
	function erroUsuarioInvalido(){
		$data["error"] = "Usuário inválido.";
		$this->load->vars($error);
		$this->load->view('priv/login/login_view');
	}
	
	function autenticar() {
		$this->form_validation->set_rules('usuarioEmail', 'Usuario', 'required');
		$this->form_validation->set_rules('senha', 'Senha', 'required');
		
		if ($this->form_validation->run() == FALSE) {
			$error = array('error' => 'Usuario e senha obrigatórios.');
			$this->load->vars($error);
			$this->load->view('priv/login/login_view');
		} else {
			$error = $this->validate();
		
			if($error == 1){
				$error = array('error' => 'Usuario inativo.');
				$this->load->vars($error);
			}
			if($error == 2){
				$error = array('error' => 'Usuario inválido.');
				$this->load->vars($error);
			}	
			$this->load->view('priv/login/login_view');
		}
	}
	
	function validate() {
		$this->load->model('Usuario_model');
		$this->Usuario_model->setLogin($this->input->post("usuarioEmail"));
		$this->Usuario_model->setSenha($this->input->post("senha"));	
		
		$usuarios = $this->Usuario_model->validate();
			
		if ($usuarios) {	
			foreach ($usuarios as $row) {
				$this->Usuario_model->setIdUsuario($row->idUsuario);
				$this->Usuario_model->setSituacao($row->situacao);
				$this->Usuario_model->setNome($row->nome);
				$this->Usuario_model->setTipo($row->tipo);
				$this->Usuario_model->setIdEmpresa($row->idEmpresa);
			}
		
			if ($this->Usuario_model->getSituacao() != "ATIVO") {
				return 1;
			}
	
			$data = array(
				'session_id' => $this->Usuario_model->getIdUsuario(),
				'idUsuario' => $this->Usuario_model->getIdUsuario(),
				'login' => $this->Usuario_model->getLogin(),
				'senha' => $this->Usuario_model->getSenha(),
				'nome' => $this->Usuario_model->getNome(),
				'tipo' => $this->Usuario_model->getTipo(),
				'idEmpresa' => $this->Usuario_model->getIdEmpresa(),
				'logged' => true
			);
			$this->session->set_userdata($data);
			$_SESSION["test"] = true;
			redirect('principal/arearestrita');
		} else {
			return 2;
		}
	}

	function logoff() {		
		$_SESSION["test"] = null;
		$this->session->sess_destroy();
		redirect($this->index());
	}

}