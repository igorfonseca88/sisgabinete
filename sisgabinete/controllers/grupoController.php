<?php header("Content-Type: text/html; charset=utf-8", true); ?>
<?php

class GrupoController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }
	
	function index()
	{
		$this->load->model('Grupo_model', 'grupo');
		$data["grupo"] = $this->grupo->getAll();
		
		$this->load->vars($data);
		$this->load->view("priv/grupo/listGrupo");
	}
	
	function novoGrupoAction()
	{
		$this->load->view("priv/grupo/addGrupo");
	}
	
	function addGrupo()
	{
		$this->load->model("Grupo_model", "grupo");		
		$insert = array(
			"titulo" => $this->input->post("titulo")
		);
		
		if ($this->grupo->add_record($insert) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["error"] = "Erro ao salvar.";
		}
		
		$data["grupo"] = $this->grupo->getAll();
		$this->load->vars($data);
		$this->load->view("priv/grupo/listGrupo");
	}
	
	function deleteGrupo($id)
	{
		$this->load->model("Grupo_model", "grupo");		

		if ($this->grupo->delete($id) > 0) {
			$data["sucesso"] = "Excluído com sucesso.";
		} else {
			$data["error"] = "Erro ao excluir.";
		}
		
		$data["grupo"] = $this->grupo->getAll();
		$this->load->vars($data);
		$this->load->view("priv/grupo/listGrupo");
	}

	function editGrupo() {
		$this->load->model("Grupo_model", "grupo");
		$id = $this->input->post("id");
		
		$update = array(
			"titulo" => $this->input->post("titulo")
		);
		
		if ($this->grupo->update($id,$update) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["erro"] = "Erro ao salvar.";
		}
		
		$data["grupo"] = $this->grupo->getAll();
		$this->load->vars($data);
		$this->load->view("priv/grupo/listGrupo");
    }
    
    function editarGrupoAction($id, $mensagem = array()) {   
        $this->load->model('Grupo_model', 'grupo');
        $data["grupo"] = $this->grupo->buscarPorId($id);
		
        $data["sucesso"] = $mensagem["sucesso"];
        $data["error"] = $mensagem["error"];
			   
        $this->load->vars($data);
        $this->load->view("priv/grupo/editGrupo");
    } 
}

?>